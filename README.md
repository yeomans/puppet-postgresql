# Puppet-Postgresql


Puppet-postgresql is a Puppet module that implements what I need to get postgresql 
working.


## Sample hiera configuration data.  

You must provide the config\_file, hba\_file, and ident_file settings if any of those files
are to be managed by Puppet.


    postgresql::version: 9.5


    # Note that you can omit any of these next three settings.  Puppet will not touch the 
    # corresponding configuration file.
    postgresql::config::pg_hba: 
    -
        comment: 'Database administrative login by Unix domain socket'
        type: 'local'
        database: 'all'
        user: 'postgres'
        method: 'peer'
        options:
            map: 'rootaspg'
    -
        comment: 'IPv4 local connection for cron'
        type: 'host'
        database: 'all'
        user: 'postgres'
        address: '127.0.0.1/32'
        method: 'trust'
    -
        comment: 'IPv4 local connections'
        type: 'host'
        database: 'all'
        user: 'all'
        address: '127.0.0.1/32'
        method: 'md5'
        
    postgresql::config::pg_ident: 
        - 
          mapname: "rootaspg"
          system-username: "postgres"
          pg-username: "postgres"
        - 
          mapname: "rootaspg"
          system-username: "root"
          pg-username: "postgres"

    postgresql::config::postgresql: 
        - '#------------------------------------------------------------------------------'
        - '# FILE LOCATIONS'
        - '#------------------------------------------------------------------------------'
        - 
            name: data_directory
            value: "/var/lib/postgresql/%{hiera('postgresql::version')}/main"
        - 
            name: config_file
            value: "%{hiera('postgresql::config::etc')}/postgresql.conf"
        - 
            name: hba_file
            value: "%{hiera('postgresql::config::etc')}/pg_hba.conf"
        - 
            name: ident_file
            value: "%{hiera('postgresql::config::etc')}/pg_ident.conf"
        - 
            name: external_pid_file
            value: "/var/run/postgresql/%{hiera('postgresql::version')}-main.pid" # (change requires restart)
        - ''
        - '#------------------------------------------------------------------------------'
        - '# CONNECTIONS AND AUTHENTICATION'
        - '#------------------------------------------------------------------------------'
        - ''
        - '# - Connection Settings -'
        - ''
        - 
            name: listen_addresses
            value: "127.0.0.1, %{::networking.interfaces.eth0:1.ip}"
        - 
            name: port 
            value: 5432 # (change requires restart)
        - 
            name: max_connections
            value: 100
        -   
            name: unix_socket_directories
            value: '/var/run/postgresql' # (change requires restart)