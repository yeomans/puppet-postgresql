class postgresql::repo(String $version)
    {    
    apt::pin
        {
        'apt.postgresql.org':
        originator => 'apt.postgresql.org',
        priority   => 500,
        }->
    apt::source 
        {
        'apt.postgresql.org':
          location => 'http://apt.postgresql.org/pub/repos/apt/',
          release => "${::lsbdistcodename}-pgdg",
          repos => "main ${version}",
        }
    apt::key
        {
        'B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8':
        source =>'http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc',
        }
    }
