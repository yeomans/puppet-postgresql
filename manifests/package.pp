class postgresql::package(
    String $version, 
    Hash $packages={}, 
    )
    {
    $all_packages = {"postgresql-$version" => {}} + $packages
    create_resources('package', $all_packages, {ensure => present, require => Exec['apt_update']})
    }
