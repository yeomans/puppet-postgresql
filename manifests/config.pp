class postgresql::config
    (
    String $version,
    # if any of these variables is set to undef, puppet will not touch the corresponding
    # configuration file.
    Optional[Array] $pg_hba=undef,
    Optional[Array] $pg_ident=undef,
    Optional[Hash] $postgresql=undef,
    )
    {
    if $pg_hba != undef
        {
        file
            {
            $postgresql[hba_file]:
            ensure => file,
            owner => 'postgres',
            group => 'postgres',
            mode => '0644',
            content => template('postgresql/pg_hba.conf.erb')
            }
        }

    if $pg_ident != undef
        {
        file
            {
            $postgresql[ident_file]:
            ensure => file,
            owner => 'postgres',
            group => 'postgres',
            mode => '0644',
            content => template('postgresql/pg_ident.conf.erb')
            }
        }

    if $postgresql != undef
        {
        file
            {
            $postgresql[config_file]:
            ensure => file,
            owner => 'postgres',
            group => 'postgres',
            mode => '0644',
            content => template('postgresql/postgresql.conf.erb')
            }
        }
    }
