class postgresql(String $version)
    {
    class{'postgresql::repo': version=>$version} ->
    class{'postgresql::package': version=>$version} ->
    class{'postgresql::config': version=>$version} ~>
    class{'postgresql::service': version=>$version} ->
    Class['postgresql']
    }
