# this is a refresh-only version of a service object.

class postgresql::service(String $version)
    {
    exec
        {
        'reload_postgresql':
        command => "/usr/bin/pg_ctlcluster $version main reload",        
        user => 'postgres',
        path =>"$path",
        refreshonly => true,
        }
    }
